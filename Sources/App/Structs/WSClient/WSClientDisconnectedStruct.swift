//
//  WSClientDisconnectedStruct.swift
//  
//
//  Created by nexsus on 6/9/20.
//

import Foundation
import NatsUtilities

struct WSClientDisconnectedStruct: NatsNotificationType {
  
  static var sub: String = "internal.ws.broadcast.v3.disconnected"
  
  var version: Int = CONSTANTS.PROTOCOL_VERSION
  
  var params: Params
  
  var method: String = "internal.ws.broadcast.v3.disconnected"
  
  struct Params: Codable {
      let accountID: AccountID
      let endpointID: EndpointID
      let sessionID: SessionID
      let registerIP: String
      var appVersion: String
      var deviceID: String
      var lastRemoteIP: String
      var platform: Platforms
      var deviceName: String?
  }
  
}

