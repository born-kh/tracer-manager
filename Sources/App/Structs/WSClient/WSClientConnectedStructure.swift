//
//  File.swift
//
//
//  Created by nexsus on 4/14/20.
//

import Foundation
import NatsUtilities

struct WSClientConnectedStruct: NatsNotificationType {
  
  static var sub: String = "internal.ws.broadcast.v3.connected"
  
  var version: Int = CONSTANTS.PROTOCOL_VERSION
  
  var params: Params
  
  var method: String = "internal.ws.broadcast.v3.connected"
  
  struct Params: Codable {
    let accountID: AccountID
    let sessionID: SessionID
    let endpointID: EndpointID
    let registerIP: String
    var appVersion: String
    var deviceID: String
    var lastRemoteIP: String
    var platform: Platforms
    var deviceName: String?
  }
  
  
}

