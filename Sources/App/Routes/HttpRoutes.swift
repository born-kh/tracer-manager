//
// HttpRoutes.swift
//  
//
//  Created by nexsus on 5/1/20.
//


import Vapor

func httpRoutes(_ app: Application) throws {
 
    
    let tracerCont = TracerController()
    
    
    app.post("searchtracer", use: tracerCont.search)
    app.post("removetracer", use: tracerCont.delete)
}

