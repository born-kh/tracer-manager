//
//  subscribers.swift
//
//
//  Created by nexsus on 4/14/20.
//
import NatsUtilities
import Vapor
func subscribers(_ app: Application) {
    print("subscribers are ready to subscribe")
    let ch = ClientHandler(app)

    app.handler.subscribe(WSClientConnectedStruct.self, callback: ch.WSClientConnected)
    app.handler.subscribe(WSClientDisconnectedStruct.self, callback: ch.WSClientDisconnected)
    
    
//    app.nats.subscribe("logs.monitoring.*.*") { (msg) in
//        ch.WSClientTracer(natsMessage: msg)
//    }.whenSuccess { (Void) in
//        print("[ NOTICE ] [\(Date())] [ NATS ]  [ NOTIFICATION ]  SUBJECT: \("logs.monitoring.*.*")")
//    }
    
    
}

