//
//  ClientHandler.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent
import NatsUtilities
import Vapor

final class ClientHandler {
    let app: Application
    
    init(_ app: Application) {
        self.app = app
    }
    func WSClientConnected(_ req: NatsNotificationModel<WSClientConnectedStruct.Params>, _ eventLoop: EventLoop) ->EventLoopFuture<Void> {
            let session_log = SessionLogs(id: nil,
                                              account_id: req.params.accountID.rawValue,
                                              session: req.params.sessionID.value,
                                              platform: req.params.platform.rawValue,
                                              device_id: req.params.deviceID,
                                              connected_at: Date(),
                                              disconnected_at: nil,
                                              enabled: true,
                                              connection_type: .WebSocket)
                return session_log.save(on: app.db).map { _ in }
    }
    
    func WSClientDisconnected(_ req: NatsNotificationModel<WSClientDisconnectedStruct.Params>, _ eventLoop: EventLoop) ->EventLoopFuture<Void> {
           let session_log = SessionLogs(id: nil,
                                                  account_id: req.params.accountID.rawValue,
                                                  session: req.params.sessionID.value,
                                                  platform: req.params.platform.rawValue,
                                                  device_id: req.params.deviceID,
                                                  connected_at: Date(),
                                                  disconnected_at: nil,
                                                  enabled: true,
                                                  connection_type: .WebSocket)
                    return session_log.save(on: app.db).map { _ in }
    }

 

    static func HTTPClientConnected(natsMessage: NatsMessage) throws -> EventLoopFuture<Void> {
        
        return natsMessage.eventLoop.makeSucceededFuture( ())
    }
    
    static func HTTPClientDisconnected(natsMessage: NatsMessage) throws -> EventLoopFuture<Void> {
        
        return natsMessage.eventLoop.makeSucceededFuture( ())
    }
    
    
    
    func WSClientTracer(natsMessage: NatsMessage) -> EventLoopFuture<Void> {
        
        do{
            let subject = natsMessage.headers.subject
            let subjectArray = subject.components(separatedBy: ".")
            let resp = try natsMessage.decode(type: TracerResponse.self)
            guard let stringPayload = String(data: natsMessage.payload, encoding: .utf8) else {
                     return natsMessage.eventLoop.future()
                 }
            
            let jsonPayload = StringToJSONB(string: stringPayload)
            let tracer_logs = TracerLogs(account_id: subjectArray[2], session_id: subjectArray[3], method: resp.method, payload: jsonPayload )
            return tracer_logs.save(on: app.db).map { _ in }
        }catch{
            return natsMessage.eventLoop.future()
        }
        
    }
}

