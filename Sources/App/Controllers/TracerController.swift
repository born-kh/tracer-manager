//
//  TracerController.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent
import Vapor

struct TracerController {
    func search(_ req: Request) throws -> EventLoopFuture<[TracerLogs]> {
        let params = try req.content.decode(ParamsFilter.self)
        return TracerLogs.query(on: req.db).group(.or)
            {$0.filter(\.$account_id, .equal, params.search)
            .filter(\.$session_id, .equal, params.search)
            .filter(\.$method, .equal, params.search)}
            .filter(\.$ts, .greaterThanOrEqual, params.fromTS)
            .filter(\.$ts, .lessThanOrEqual, params.toTS)
            .all()
    }

    func delete(_ req: Request) throws -> EventLoopFuture<HTTPStatus> {
        let params = try req.content.decode(ParamsDelete.self)
        return TracerLogs.query(on: req.db)
            .filter(\.$account_id == params.accountID)
            .delete().map { .ok }
    }
}

struct ParamsFilter: Codable, Content {
    let search: String
    let fromTS: Date
    let toTS: Date
}

struct ParamsDelete: Codable, Content {
    let accountID: String
}

