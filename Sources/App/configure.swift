import Fluent
import FluentPostgresDriver
import Nats
import NatsUtilities
import Vapor

// Called before your application initializes.
public func configure(_ app: Application) throws {
    // Serves files from `Public/` directory
    // app.middleware.use(FileMiddleware(publicDirectory: app.directory.publicDirectory))

    // Configure SQLite database

    
    let corsConfiguration = CORSMiddleware.Configuration(
        allowedOrigin: .all,
        allowedMethods: [.GET, .POST, .PUT, .OPTIONS, .DELETE, .PATCH],
        allowedHeaders: [.accept, .authorization, .contentType, .origin, .xRequestedWith, .userAgent, .accessControlAllowOrigin, .uaMedia]
    )
    let cors = CORSMiddleware(configuration: corsConfiguration)
    let error = ErrorMiddleware.default(environment: app.environment)
    let file = FileMiddleware(publicDirectory: app.directory.publicDirectory)
    app.middleware.use(file)
    app.middleware = .init()
    app.middleware.use(cors)
    app.middleware.use(error)
    
    app.databases.use(.postgres(

        hostname: Environment.get("POSTGRES_HOST") ?? "10.7.7.117",
        username: Environment.get("POSTGRES_USERNAME") ?? "logger",
        password: Environment.get("POSTGRES_PASSWORD") ?? "q97LY0Uzrz3y",
        database: Environment.get("POSTGRES_DATABASE") ?? "session_manager"
    ), as: .psql)

    let NATS_CLUSTER = Environment.get("NATS_CLUSTER") ?? "10.7.7.137"
    let NATS_TOKEN = Environment.get("NATS_TOKEN")

    // NATS Configuration
    app.nats.configuration = NatsConfiguration(
        host: NATS_CLUSTER,
        disBehavior: .fatalCrash,
        clusterName: nil,
        streaming: false,
        auth_token: NATS_TOKEN,
        onOpen: app.router.onOpen,
        onStreamingOpen: app.router.onStreamingOpen,
        onClose: app.router.onClose(conn:),
        onError: app.router.onError
    )

    app.handler = NatsHandler(app, moduleName: CONSTANTS.INSTANCE.NAME, moduleId: CONSTANTS.INSTANCE.ID)
    app.migrations.add(CreateTracerLogs())

    app.migrations.add(CreateSessionLogs())

    try app.autoMigrate().wait()


    try httpRoutes(app)

}

extension Application {
    var router: NatsRouter {
        if let existing = self.storage[NatsRouterKey.self] {
            return existing
        } else {
            let new = NatsRouter(self)
            storage[NatsRouterKey.self] = new
            return new
        }
    }

    struct NatsRouterKey: StorageKey {
        typealias Value = NatsRouter
    }
}

class NatsRouter {
    let app: Application
    
    init(_ app: Application) {
        self.app = app
    }
    
    func onOpen(conn: NatsConnection) {
        print("[ NATS ] [\(Date())] 🥒 Connected.")
        print("[ NATS ] Initializing subscriptions...")
        subscribers(app)
    }
    
    func onStreamingOpen(conn: NatsConnection) {
        
    }
    func onClose(conn: NatsConnection) {
        print("[ NATS ] [\(Date())] 🌶 Disconected.")
    }
    func onError(conn: NatsConnection, error: Error) {
        
    }
}
