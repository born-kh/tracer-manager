//
//  SessionLogs.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent
import Vapor

final class SessionLogs: Model, Content {
    static let schema = "session_logs"

    @ID(key: .id) var id: UUID?
    @Field(key: .account_id) var account_id: String
    @Field(key: .session) var session: String
    @Field(key: .platform) var platform: String
    @Field(key: .device_id) var device_id: String
    @Field(key: .connected_at) var connected_at: Date?
    @Field(key: .disconnected_at) var disconnected_at: Date?
    @Field(key: .enabled) var enabled: Bool?
    @Enum(key: .connection_type) var connection_type: ConnectionType

    init() {}
    init(id: UUID? = nil, account_id: String, session: String, platform: String, device_id: String, connected_at: Date? = nil, disconnected_at: Date? = nil, enabled: Bool? = nil, connection_type: ConnectionType) {
        self.id = id
        self.account_id = account_id
        self.session = session
        self.platform = platform
        self.device_id = device_id
        self.connected_at = connected_at
        self.disconnected_at = disconnected_at
        self.enabled = enabled
        self.connection_type = connection_type
    }

    enum ConnectionType: String, Codable, CaseIterable {
        static var name: FieldKey { .connection_type }
        case WebSocket
        case HTTP
    }
}

extension FieldKey {
   

    static var session: Self { "session" }
    static var platform: Self { "platform" }
    static var device_id: Self { "device_id" }
    static var connected_at: Self { "connected_at" }
    static var disconnected_at: Self { "disconnected_at" }
    static var enabled: Self { "enabled" }
    static var connection_type: Self { "connection_type" }
 
}
