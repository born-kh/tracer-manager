//
//  TracerLogs.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent
import Vapor
import FluentPostgresDriver



final class TracerLogs: Model, Content {
    static let schema = "tracer_logs"
    @ID(key: .id) var id: UUID?
    @Field(key: .account_id) var account_id: String
    @Field(key: .session_id) var session_id: String
    @Field(key: .method) var method: String
    @Field(key: .payload) var payload: StringToJSONB
    @Field(key: .ts) var ts: Date?
    
    init() {}
    
    init(id: UUID? = nil, account_id: String, session_id: String, method: String, payload: StringToJSONB, ts: Date? = Date()) {
        self.id = id
        self.account_id = account_id
        self.session_id = session_id
        self.method = method
        self.payload = payload
        self.ts = ts
    }
}


struct TracerResponse: Codable{
    let id: UUID
    let version: Int
    let method: String
}

struct StringToJSONB: Codable {
    var string: String
}

extension StringToJSONB: PostgresDataConvertible {
    static var postgresDataType: PostgresDataType {
        .jsonb
    }

    init?(postgresData: PostgresData) {
        guard let data = postgresData.jsonb else {
            return nil
        }
        self.string = .init(decoding: data, as: UTF8.self)
    }

    var postgresData: PostgresData? {
        .init(jsonb: .init(self.string.utf8))
    }
}
extension FieldKey {
    static var account_id: Self { "account_id" }
    static var session_id: Self { "session_id" }
    static var payload: Self { "payload" }
    static var ts: Self { "ts" }
    static var method: Self {"method"}
    
}
