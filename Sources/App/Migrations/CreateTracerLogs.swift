//
//  CreateTracerLogs.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent

struct CreateTracerLogs: Migration {
    
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema(TracerLogs.schema)
            .id()
            .field(.account_id, .string, .required)
            .field(.session_id, .string, .required)
            .field(.method, .string, .required)
            .field(.payload, .custom("JSONB"), .required)
            .field(.ts, .datetime)
            .create()
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(TracerLogs.schema).delete()
    }
}

