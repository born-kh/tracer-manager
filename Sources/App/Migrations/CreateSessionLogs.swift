//
//  CreateSessionLogs.swift
//  
//
//  Created by nexsus on 7/13/20.
//

import Fluent

struct CreateSessionLogs: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        var enumBuilder = database.enum(SessionLogs.ConnectionType.name.description)
        for option in SessionLogs.ConnectionType.allCases {
            enumBuilder = enumBuilder.case(option.rawValue)
        }
        return enumBuilder.create()
            .flatMap { connectionType in
                database.schema(SessionLogs.schema)
                    .id()
                    .field(.account_id, .string, .required)
                    .field(.session, .string, .required)
                    .field(.platform, .string, .required)
                    .field(.device_id, .string, .required)
                    .field(.connected_at, .datetime)
                    .field(.connection_type, connectionType, .required)
                    .field(.disconnected_at, .datetime)
                    .field(.enabled, .bool)
                    .create()
        }
    }
    
    func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(SessionLogs.schema).delete().flatMap {
            database.enum(SessionLogs.ConnectionType.name.description).delete()
        }
    }
}
