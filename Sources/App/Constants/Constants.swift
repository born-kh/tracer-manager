//
//  CONSTANTS.swift
//
//
//  Created by nexsus on 4/14/20.
//

import Vapor

struct CONSTANTS {
    struct INSTANCE {
        public static let NAME: String = "monitoring"
        public static let ID: UUID = UUID()
    }
        static let PROTOCOL_VERSION: Int = 3

}
